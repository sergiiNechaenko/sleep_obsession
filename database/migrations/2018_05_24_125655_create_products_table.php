<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->unsignedInteger('type_id');
            $table->unsignedInteger('status_id');
            $table->unsignedInteger('year_id');
            $table->text('image');
            $table->text('image_150x225');
            $table->text('description');
            $table->text('video');
            $table->timestamps();

            $table->foreign('type_id')->references('id')->on('product_types')
                ->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('status_id')->references('id')->on('product_statuses')
                ->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('year_id')->references('id')->on('years')
                ->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('products', function (Blueprint $table) {
            $table->dropForeign('products_type_id_foreign');
            $table->dropForeign('products_status_id_foreign');
            $table->dropForeign('products_year_id_foreign');
        });
        Schema::dropIfExists('products');
    }
}
