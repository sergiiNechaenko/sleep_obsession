<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductCountryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_country', function (Blueprint $table) {
            $table->unsignedInteger('product_id');
            $table->unsignedInteger('country_id');
            $table->timestamps();

            $table->foreign('product_id')->references('id')->on('products')
                ->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('country_id')->references('id')->on('countries')
                ->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('product_country', function (Blueprint $table) {
            $table->dropForeign('product_country_product_id_foreign');
            $table->dropForeign('product_country_country_id_foreign');
        });
        Schema::dropIfExists('product_country');
    }
}
