<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductGenreTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_genre', function (Blueprint $table) {
            $table->unsignedInteger('product_id');
            $table->unsignedInteger('genre_id');
            $table->timestamps();

            $table->foreign('product_id')->references('id')->on('products')
                ->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('genre_id')->references('id')->on('genres')
                ->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('product_genre', function (Blueprint $table) {
            $table->dropForeign('product_genre_product_id_foreign');
            $table->dropForeign('product_genre_genre_id_foreign');
        });
        Schema::dropIfExists('product_genre');
    }
}
