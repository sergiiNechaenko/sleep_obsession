<?php

use Illuminate\Database\Seeder;

class ProductStatusesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0');

        DB::table('product_statuses')->truncate();
        DB::table('product_statuses')->insert([
            'id' => 1,
            'name' => 'Normal',
            'slug' => 'normal',
        ]);
        DB::table('product_statuses')->insert([
            'id' => 2,
            'name' => 'Coming soon',
            'slug' => 'coming_soon',
        ]);
        DB::table('product_statuses')->insert([
            'id' => 3,
            'name' => 'New',
            'slug' => 'new',
        ]);
        DB::table('product_statuses')->insert([
            'id' => 4,
            'name' => 'Old',
            'slug' => 'old',
        ]);

        DB::statement('SET FOREIGN_KEY_CHECKS=1');
    }
}
