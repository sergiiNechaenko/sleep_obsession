<?php

use Illuminate\Database\Seeder;

class ProductTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0');

        DB::table('product_types')->truncate();
        DB::table('product_types')->insert([
            'id' => 1,
            'name' => 'Фильмы',
            'slug' => 'film',
        ]);
        DB::table('product_types')->insert([
            'id' => 2,
            'name' => 'Сериалы',
            'slug' => 'tv_serial',
        ]);

        DB::statement('SET FOREIGN_KEY_CHECKS=1');
    }
}
