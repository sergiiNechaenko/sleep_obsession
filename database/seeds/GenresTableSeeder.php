<?php

use Illuminate\Database\Seeder;

class GenresTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0');

        DB::table('genres')->truncate();
        DB::table('genres')->insert([
            'id' => 1,
            'name' => 'Ужасы',
            'slug' => 'horror',
        ]);
        DB::table('genres')->insert([
            'id' => 2,
            'name' => 'Детектив',
            'slug' => 'detective',
        ]);
        DB::table('genres')->insert([
            'id' => 3,
            'name' => 'Фантастика',
            'slug' => 'fantasy',
        ]);
        DB::table('genres')->insert([
            'id' => 4,
            'name' => 'Комедия',
            'slug' => 'comedy',
        ]);
        DB::table('genres')->insert([
            'id' => 5,
            'name' => 'Приключения',
            'slug' => 'adventure',
        ]);
        DB::table('genres')->insert([
            'id' => 6,
            'name' => 'Триллер',
            'slug' => 'Thriller',
        ]);
        DB::table('genres')->insert([
            'id' => 7,
            'name' => 'Драмма',
            'slug' => 'drama',
        ]);
        DB::table('genres')->insert([
            'id' => 8,
            'name' => 'Криминал',
            'slug' => 'criminal',
        ]);
        DB::table('genres')->insert([
            'id' => 9,
            'name' => 'Семейный',
            'slug' => 'family',
        ]);
        DB::table('genres')->insert([
            'id' => 10,
            'name' => 'Мелодрама',
            'slug' => 'melodrama',
        ]);
        DB::table('genres')->insert([
            'id' => 11,
            'name' => 'Боевик',
            'slug' => 'fight',
        ]);


        DB::statement('SET FOREIGN_KEY_CHECKS=1');
    }
}
