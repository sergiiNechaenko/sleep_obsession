<?php

use Illuminate\Database\Seeder;

class ProductGenreTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0');

        DB::table('product_genre')->truncate();
        DB::table('product_genre')->insert([
            'product_id' => 1,
            'genre_id' => 1,
        ]);
        DB::table('product_genre')->insert([
            'product_id' => 1,
            'genre_id' => 3,
        ]);
        DB::table('product_genre')->insert([
            'product_id' => 1,
            'genre_id' => 6,
        ]);

        DB::table('product_genre')->insert([
            'product_id' => 2,
            'genre_id' => 6,
        ]);
        DB::table('product_genre')->insert([
            'product_id' => 2,
            'genre_id' => 7,
        ]);
        DB::table('product_genre')->insert([
            'product_id' => 2,
            'genre_id' => 8,
        ]);

        DB::table('product_genre')->insert([
            'product_id' => 3,
            'genre_id' => 6,
        ]);
        DB::table('product_genre')->insert([
            'product_id' => 3,
            'genre_id' => 5,
        ]);
        DB::table('product_genre')->insert([
            'product_id' => 3,
            'genre_id' => 3,
        ]);

        DB::table('product_genre')->insert([
            'product_id' => 4,
            'genre_id' => 6,
        ]);

        DB::table('product_genre')->insert([
            'product_id' => 5,
            'genre_id' => 6,
        ]);
        DB::table('product_genre')->insert([
            'product_id' => 5,
            'genre_id' => 3,
        ]);

        DB::table('product_genre')->insert([
            'product_id' => 6,
            'genre_id' => 3,
        ]);
        DB::table('product_genre')->insert([
            'product_id' => 6,
            'genre_id' => 5,
        ]);

        DB::table('product_genre')->insert([
            'product_id' => 7,
            'genre_id' => 6,
        ]);
        DB::table('product_genre')->insert([
            'product_id' => 7,
            'genre_id' => 4,
        ]);
        DB::table('product_genre')->insert([
            'product_id' => 7,
            'genre_id' => 3,
        ]);

        DB::table('product_genre')->insert([
            'product_id' => 8,
            'genre_id' => 6,
        ]);
        DB::table('product_genre')->insert([
            'product_id' => 8,
            'genre_id' => 3,
        ]);
        DB::table('product_genre')->insert([
            'product_id' => 8,
            'genre_id' => 9,
        ]);

        DB::table('product_genre')->insert([
            'product_id' => 9,
            'genre_id' => 6,
        ]);
        DB::table('product_genre')->insert([
            'product_id' => 9,
            'genre_id' => 3,
        ]);

        DB::table('product_genre')->insert([
            'product_id' => 10,
            'genre_id' => 6,
        ]);
        DB::table('product_genre')->insert([
            'product_id' => 10,
            'genre_id' => 3,
        ]);

        DB::table('product_genre')->insert([
            'product_id' => 11,
            'genre_id' => 6,
        ]);
        DB::table('product_genre')->insert([
            'product_id' => 11,
            'genre_id' => 7,
        ]);
        DB::table('product_genre')->insert([
            'product_id' => 11,
            'genre_id' => 1,
        ]);

        DB::table('product_genre')->insert([
            'product_id' => 12,
            'genre_id' => 4,
        ]);
        DB::table('product_genre')->insert([
            'product_id' => 12,
            'genre_id' => 10,
        ]);

        DB::table('product_genre')->insert([
            'product_id' => 13,
            'genre_id' => 8,
        ]);
        DB::table('product_genre')->insert([
            'product_id' => 13,
            'genre_id' => 7,
        ]);
        DB::table('product_genre')->insert([
            'product_id' => 13,
            'genre_id' => 2,
        ]);
        DB::table('product_genre')->insert([
            'product_id' => 13,
            'genre_id' => 6,
        ]);
        DB::table('product_genre')->insert([
            'product_id' => 13,
            'genre_id' => 11,
        ]);

        DB::table('product_genre')->insert([
            'product_id' => 14,
            'genre_id' => 7,
        ]);
        DB::table('product_genre')->insert([
            'product_id' => 14,
            'genre_id' => 5,
        ]);
        DB::table('product_genre')->insert([
            'product_id' => 14,
            'genre_id' => 2,
        ]);
        DB::table('product_genre')->insert([
            'product_id' => 14,
            'genre_id' => 3,
        ]);
        DB::table('product_genre')->insert([
            'product_id' => 14,
            'genre_id' => 6,
        ]);

        DB::table('product_genre')->insert([
            'product_id' => 15,
            'genre_id' => 2,
        ]);
        DB::table('product_genre')->insert([
            'product_id' => 15,
            'genre_id' => 7,
        ]);
        DB::table('product_genre')->insert([
            'product_id' => 15,
            'genre_id' => 3,
        ]);

        DB::table('product_genre')->insert([
            'product_id' => 16,
            'genre_id' => 6,
        ]);
        DB::table('product_genre')->insert([
            'product_id' => 16,
            'genre_id' => 7,
        ]);
        DB::table('product_genre')->insert([
            'product_id' => 16,
            'genre_id' => 8,
        ]);

        DB::table('product_genre')->insert([
            'product_id' => 17,
            'genre_id' => 3,
        ]);
        DB::table('product_genre')->insert([
            'product_id' => 17,
            'genre_id' => 6,
        ]);
        DB::table('product_genre')->insert([
            'product_id' => 17,
            'genre_id' => 7,
        ]);
        DB::table('product_genre')->insert([
            'product_id' => 17,
            'genre_id' => 11,
        ]);

        DB::table('product_genre')->insert([
            'product_id' => 18,
            'genre_id' => 7,
        ]);
        DB::table('product_genre')->insert([
            'product_id' => 18,
            'genre_id' => 4,
        ]);

        DB::statement('SET FOREIGN_KEY_CHECKS=1');
    }
}
