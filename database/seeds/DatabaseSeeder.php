<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(ProductTypesTableSeeder::class);
        $this->call(ProductStatusesTableSeeder::class);
        $this->call(GenresTableSeeder::class);
        $this->call(CountriesTableSeeder::class);
        $this->call(YearsTableSeeder::class);
        $this->call(ProductsTableSeeder::class);
        $this->call(HeaderPostersTableSeeder::class);
        $this->call(ProductGenreTableSeeder::class);
        $this->call(ProductCountryTableSeeder::class);
    }
}
