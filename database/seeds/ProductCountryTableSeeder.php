<?php

use Illuminate\Database\Seeder;

class ProductCountryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0');

        DB::table('product_country')->truncate();
        DB::table('product_country')->insert([
            'product_id' => 1,
            'country_id' => 1,
        ]);

        DB::table('product_country')->insert([
            'product_id' => 2,
            'country_id' => 1,
        ]);

        DB::table('product_country')->insert([
            'product_id' => 3,
            'country_id' => 1,
        ]);
        DB::table('product_country')->insert([
            'product_id' => 3,
            'country_id' => 4,
        ]);

        DB::table('product_country')->insert([
            'product_id' => 4,
            'country_id' => 1,
        ]);
        DB::table('product_country')->insert([
            'product_id' => 4,
            'country_id' => 3,
        ]);
        DB::table('product_country')->insert([
            'product_id' => 4,
            'country_id' => 5,
        ]);

        DB::table('product_country')->insert([
            'product_id' => 5,
            'country_id' => 1,
        ]);

        DB::table('product_country')->insert([
            'product_id' => 6,
            'country_id' => 1,
        ]);
        DB::table('product_country')->insert([
            'product_id' => 6,
            'country_id' => 6,
        ]);

        DB::table('product_country')->insert([
            'product_id' => 7,
            'country_id' => 1,
        ]);

        DB::table('product_country')->insert([
            'product_id' => 8,
            'country_id' => 1,
        ]);

        DB::table('product_country')->insert([
            'product_id' => 9,
            'country_id' => 1,
        ]);

        DB::table('product_country')->insert([
            'product_id' => 10,
            'country_id' => 1,
        ]);
        DB::table('product_country')->insert([
            'product_id' => 10,
            'country_id' => 6,
        ]);

        DB::table('product_country')->insert([
            'product_id' => 11,
            'country_id' => 1,
        ]);

        DB::table('product_country')->insert([
            'product_id' => 12,
            'country_id' => 1,
        ]);

        DB::table('product_country')->insert([
            'product_id' => 13,
            'country_id' => 1,
        ]);
        DB::table('product_country')->insert([
            'product_id' => 13,
            'country_id' => 6,
        ]);

        DB::table('product_country')->insert([
            'product_id' => 14,
            'country_id' => 1,
        ]);

        DB::table('product_country')->insert([
            'product_id' => 15,
            'country_id' => 1,
        ]);

        DB::table('product_country')->insert([
            'product_id' => 16,
            'country_id' => 1,
        ]);

        DB::table('product_country')->insert([
            'product_id' => 17,
            'country_id' => 1,
        ]);

        DB::table('product_country')->insert([
            'product_id' => 18,
            'country_id' => 1,
        ]);

        DB::statement('SET FOREIGN_KEY_CHECKS=1');
    }
}
