<?php

use Illuminate\Database\Seeder;

class HeaderPostersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0');

        DB::table('header_posters')->truncate();
        DB::table('header_posters')->insert([
            'id' => 1,
            'product_id' => 2,
            'image' => 'images/shotcaller_poster.jpg',
            'priority' => 2,
        ]);

        DB::table('header_posters')->insert([
            'id' => 2,
            'product_id' => 8,
            'image' => 'images/realsteel_poster.jpg',
            'priority' => 8,
        ]);

        DB::table('header_posters')->insert([
            'id' => 3,
            'product_id' => 4,
            'image' => 'images/john_poster.jpg',
            'priority' => 1,
        ]);

        DB::table('header_posters')->insert([
            'id' => 4,
            'product_id' => 3,
            'image' => 'images/matrix_poster.jpg',
            'priority' => 4,
        ]);

        DB::table('header_posters')->insert([
            'id' => 5,
            'product_id' => 9,
            'image' => 'images/spiderman3_poster.jpg',
            'priority' => 8,
        ]);

        DB::table('header_posters')->insert([
            'id' => 6,
            'product_id' => 5,
            'image' => 'images/elizium_poster.jpg',
            'priority' => 6,
        ]);

        DB::table('header_posters')->insert([
            'id' => 7,
            'product_id' => 6,
            'image' => 'images/avatar_poster.jpg',
            'priority' => 7,
        ]);

        DB::table('header_posters')->insert([
            'id' => 8,
            'product_id' => 10,
            'image' => 'images/riddik_poster.jpg',
            'priority' => 3,
        ]);

        DB::table('header_posters')->insert([
            'id' => 9,
            'product_id' => 18,
            'image' => 'images/red_poster.jpg',
            'priority' => 1,
        ]);

        DB::table('header_posters')->insert([
            'id' => 10,
            'product_id' => 16,
            'image' => 'images/bad_poster.jpg',
            'priority' => 2,
        ]);

        DB::table('header_posters')->insert([
            'id' => 11,
            'product_id' => 15,
            'image' => 'images/100_poster.jpg',
            'priority' => 3,
        ]);

        DB::table('header_posters')->insert([
            'id' => 12,
            'product_id' => 17,
            'image' => 'images/ship_poster.jpg',
            'priority' => 4,
        ]);

        DB::table('header_posters')->insert([
            'id' => 13,
            'product_id' => 13,
            'image' => 'images/prison_break_poster.jpg',
            'priority' => 5,
        ]);

        DB::table('header_posters')->insert([
            'id' => 14,
            'product_id' => 11,
            'image' => 'images/walking_dead_poster.jpg',
            'priority' => 6,
        ]);

        DB::table('header_posters')->insert([
            'id' => 15,
            'product_id' => 14,
            'image' => 'images/lost_poster.jpg',
            'priority' => 7,
        ]);

        DB::table('header_posters')->insert([
            'id' => 16,
            'product_id' => 12,
            'image' => 'images/big_bang_poster.jpg',
            'priority' => 8,
        ]);
        DB::statement('SET FOREIGN_KEY_CHECKS=1');
    }
}
