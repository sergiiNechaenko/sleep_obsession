<?php

use Illuminate\Database\Seeder;

class YearsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0');

        DB::table('years')->truncate();

        for ($i = 1950; $i < 2050; $i++)
            DB::table('years')->insert([
                'id' => $i-1949,
                'number' => $i,
            ]);

        DB::statement('SET FOREIGN_KEY_CHECKS=1');
    }
}
