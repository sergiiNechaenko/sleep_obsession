<?php

use Illuminate\Database\Seeder;

class CountriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0');

        DB::table('countries')->truncate();
        DB::table('countries')->insert([
            'id' => 1,
            'name' => 'Сша',
            'slug' => 'usa',
        ]);
        DB::table('countries')->insert([
            'id' => 2,
            'name' => 'Франция',
            'slug' => 'france',
        ]);
        DB::table('countries')->insert([
            'id' => 3,
            'name' => 'Китай',
            'slug' => 'china',
        ]);
        DB::table('countries')->insert([
            'id' => 4,
            'name' => 'Австралия',
            'slug' => 'australia',
        ]);
        DB::table('countries')->insert([
            'id' => 5,
            'name' => 'Канада',
            'slug' => 'canada',
        ]);
        DB::table('countries')->insert([
            'id' => 6,
            'name' => 'Великобритания',
            'slug' => 'great_britain',
        ]);

        DB::statement('SET FOREIGN_KEY_CHECKS=1');
    }
}
