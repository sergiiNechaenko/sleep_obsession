<!DOCTYPE HTML>
<html>
    <head>
        <title>Best Movie</title>
        <link rel="stylesheet" type="text/css" href="{{ asset('css/index.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ asset('css/pagination.css') }}">

        <link rel="stylesheet" type="text/css" href="{{ asset('css/main.css') }}">

        <link rel="shortcut icon" href="{{ asset('/images/favicon.ico') }}" type="image/x-icon">

        @stack('header-styles')
    </head>
    <body>

    @include('layouts.header')

    @yield('content')

    @include('layouts.footer')

    </body>
</html>