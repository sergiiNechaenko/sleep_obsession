<div class="site-header">
    <img class="filmstrip" src="{{asset('images/film_filmstrip_light.png')}}" />

    @if(count($header_posters) >= 8)
        @for($i = 0;$i < 8; $i++)
            <img src="{{asset($header_posters[$i]->image)}}" class="posters poster{{$i+1}}">
            <a href="/{{$header_posters[$i]->product->type->slug}}/{{$header_posters[$i]->product->id}}"><img src="{{asset('images/poster_light.png')}}" class="posters poster{{$i+1}} poster_light"></a>
        @endfor
    @endif

    @foreach($product_types as $product_type)
        <a href="/{{$product_type->slug}}"><p class="item_logo_{{$product_type->id}}">{{$product_type->name}}</p></a>
    @endforeach

        <input spellcheck="false" placeholder="Поиск" type="text" value="" class="search-product" id="search-product">
</div>

<script>
    var search_route = '{{ route('api.product.search',['key'=> null]) }}';
</script>

<style>
    body{
        background-image: url("{{ asset("images/film_background.png") }}");
    }
</style>