@extends ('layouts.app')

@section('content')

    <div class="container">

        {{ Form::open(array('route' => array('main.page', $product_slug ))) }}
        <div class="selectdiv">
            <select name="genre_id" class="genres" onchange="this.form.submit()">
                <option value="all">Все жанры</option>

                @foreach($genres as $genre)

                    <option value="{{$genre->id}}"
                            @if($current_genre_id == $genre->id)
                                selected
                            @endif
                    >{{$genre->name}}</option>
                @endforeach
            </select>
        </div>
        {{ Form::close() }}

        <div class="product_items">
            @foreach($products as $product)
                <div class="product_item">
                    <a href="{{$product->type->slug}}/{{$product->id}}"><img src="{{asset($product->image_150x225)}}" class="product_image" ></a>
                    <a href="{{$product->type->slug}}/{{$product->id}}"><p>{{$product->title}}</p></a>
                </div>
            @endforeach
        </div>

        {{ $products->links() }}

    </div>
@endsection

