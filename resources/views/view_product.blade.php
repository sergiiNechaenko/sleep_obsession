@extends ('layouts.app')

@section('content')
    <div class="container">
        <img class="product_image" src="{{asset($product->image)}}">

        <p class="product_name">Название фильма:</p>
        <p class="product_name1">{{$product->title}}</p>

        <p class="product_year">Год выхода:</p>
        <p class="product_year1">{{$product->year->number}}</p>

        <p class="product_country">Страна:</p>
        <p class="product_country1">
            @foreach($product->countries as $country)
                {{$country->name.' '}}
                &nbsp
            @endforeach

                @if(count($product->countries)==0)
                    Пусто
                @endif
        </p>

        <p class="product_genre">Жанр:</p>
        <p class="product_genre1">
            @foreach($product->genres as $genre)
                {{$genre->name.' '}}
                &nbsp
            @endforeach

            @if(count($product->genres)==0)
                Пусто
            @endif
        </p>

        <p class="product_description">Описание:</p>
        <p class="product_description1">{{$product->description}}</p>

        <p class="watch_online">Смотреть трейлер</p>

        <iframe class="trailer" src="{{$product->video}}" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

    </div>

@endsection

@push('header-styles')
    <link rel="stylesheet" type="text/css" href="{{ asset('css/view_product.css') }}"/>
@endpush