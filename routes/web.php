<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/{product_slug}/{product_id}', 'ProductController@view')->where('product_id', '[0-9]+');

Route::post('/{product_slug}', 'ProductController@index')->name("main.page");

Route::get('/{product_slug}', 'ProductController@index')->name("main.page");

Route::get('/', function (){
    return redirect('/film');
});