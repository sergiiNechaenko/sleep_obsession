$(document).ready(function () {

    $('#search-product').bind("keyup", function () {

        if(this.value.length >= 3) {

            $.ajax({
                url: search_route + '/' + this.value,
                success: function (result) {
                    var len = result.length;
                    var str = '';

                    for (var i = 0; i < len; i++) {
                        str += '<a href="/'+ result[i].typeSlug + '/' + result[i].id + '"><li><span class="search-title">' + result[i].title + '</span><span class="search-year">'+ result[i].yearNumber +'</span></li></a>';
                    }

                    $("#search-result").html(str);
                }
            });

        }

    });
});
