<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Year extends Model
{
    protected $fillable = [
        'number'
    ];

    public function products()
    {
        return $this->hasMany('App\Product', 'year_id');
    }
}
