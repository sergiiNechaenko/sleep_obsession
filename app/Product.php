<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = [
        'title',
        'type_id',
        'status_id',
        'genre_id',
        'year_id',
        'image',
        'image_150x225',
        'description',
        'video'
    ];

    public function type()
    {
        return $this->belongsTo('App\ProductType','type_id');
    }

    public function status()
    {
        return $this->belongsTo('App\ProductStatus','status_id');
    }

    public function genres()
    {
        return $this->belongsToMany('App\Genre','product_genre');
    }

    public function countries()
    {
        return $this->belongsToMany('App\Country','product_country');
    }

    public function year()
    {
        return $this->belongsTo('App\Year','year_id');
    }
}
