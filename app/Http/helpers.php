<?php
use App\ProductType;

function check_product_slug($product_slug){
    $product_slugs = [];
    foreach (ProductType::select('slug')->get() as $slug_item) {
        $product_slugs [] = $slug_item->slug;
    }
    if(!in_array($product_slug,$product_slugs)){
        return false;
    };
    return true;
}