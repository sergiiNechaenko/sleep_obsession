<?php

namespace App\Http\Controllers;

use App\HeaderPoster;
use App\Product;
use App\ProductType;
use App\Genre;
use Illuminate\Http\Request;


class ProductController extends Controller
{
    public function index($product_slug, Request $request)
    {
        if(!check_product_slug($product_slug)){
            return redirect('/');
        }

        $genres = Genre::all();

        $product_type = ProductType::where("slug",'=',$product_slug)->first();
        $product_types = ProductType::all();

        $header_posters = HeaderPoster::productType($product_type->id)->orderBy('priority')->get();

        $products  = Product::where('type_id','=',$product_type->id);

        $current_genre_id = $request->input('genre_id');
        if(!is_null($current_genre_id) && $current_genre_id != 'all'){
            $products = $products->whereHas('genres', function ($query) use ($current_genre_id) {
                $query->where('id', '=', $current_genre_id);
            });
        };

        $products = $products->paginate(32);

        return view('index', compact('header_posters','products','product_type','product_types','genres','product_slug','current_genre_id'));
    }

    public function view($product_slug,$product_id)
    {
        if(!check_product_slug($product_slug)){
            return redirect('/');
        }

        $product_type = ProductType::where("slug",'=',$product_slug)->first();
        $product_types = ProductType::all();

        $header_posters = HeaderPoster::productType($product_type->id)->orderBy('priority')->get();

        $product = Product::find($product_id);

        if(is_null($product))
            return redirect('/');

        return view('view_product', compact('header_posters','product','product_type','product_types'));
    }
}
