<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Product;


class ProductController extends Controller
{
    public function search($key)
    {
        $products = Product::where('title', 'LIKE', '%'.$key.'%')->get();
        foreach ($products as $product){
            $product->typeSlug = $product->type->slug;
            $product->yearNumber = $product->year->number;
        }

        return response()
            ->json($products);
    }
}
