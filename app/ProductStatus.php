<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductStatus extends Model
{
    protected $fillable = [
        'name',
        'slug'
    ];

    public function products()
    {
        return $this->hasMany('App\Product', 'status_id');
    }
}
