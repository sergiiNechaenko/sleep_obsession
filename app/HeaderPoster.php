<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HeaderPoster extends Model
{
    protected $fillable = [
        'product_id',
        'image',
        'priority'
    ];

    public function product()
    {
        return $this->belongsTo('App\Product','product_id');
    }

    public function scopeProductType($query,$product_type_id)
    {
        return $query->whereHas('product', function ($product_query)use($product_type_id) {
            $product_query->where('type_id','=',$product_type_id);
        });
    }
}
